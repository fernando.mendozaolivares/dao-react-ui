'use client'

import setupDesignSystem from "q2-design-system"
import { useEffect } from "react"

export default function Home() {
  useEffect(() => {
    setupDesignSystem()
  })
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <div className="z-10 w-full max-w-5xl items-center justify-between font-mono text-sm lg:flex">
        <q2-btn intent="workflow-primary">My Button</q2-btn>
      </div>
    </main>
  )
}
