import * as React from 'react'
import * as ReactDOM from 'react-dom/client'
import App from './App'
import setupDesignSystem from 'q2-design-system'
import './index.css'


setupDesignSystem().then(() => {
  ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
  )
})
