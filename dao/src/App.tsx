import { useEffect } from 'react'
import { healthCheck } from './services/groService'
import './App.css'
import StandardLayout from './components/layouts/standard/StandardLayout'
import Display from './components/renderEngine/Display'

function App() {
  useEffect(() => {
    healthCheck().then((data) => console.log(data.data.message))
  }, [])

  return (
    <StandardLayout>
      <Display></Display>
    </StandardLayout>
  )
}

export default App
