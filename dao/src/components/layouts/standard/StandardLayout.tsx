import { ReactNode } from 'react'
import StandardHeader from './StandardHeader'
import StandardBody from './StandardBody'
import StandardFooter from './StandardFooter'
import '@/css/layouts.css'

interface Props {
  children: ReactNode
}

const StandardLayout = ({ children }: Props) => {
  return (
    <>
      <div className="standardLayout">
        <StandardHeader></StandardHeader>
        <StandardBody>{children}</StandardBody>
        <StandardFooter></StandardFooter>
      </div>
    </>
  )
}

export default StandardLayout
