import { ReactNode } from 'react'

interface Props {
  children: ReactNode
}

const StandardBody = ({ children }: Props) => {
  return <>{children}</>
}

export default StandardBody
