import axios from 'axios'

const groService = axios.create({
  baseURL: 'http://localhost:9902',
  timeout: 1000,
})

export function healthCheck() {
  return groService.get('/healthcheck')
}
